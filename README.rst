=================
openwrt-protonvpn
=================

Configuration files and code for OpenWRT router and ProtonVPN.

.. code::

   mdecrypt backup-OpenWrt-novpn-2020-09-17.tar.gz.nc
   mdecrypt backup-OpenWrt-protonvpn-ch-dk-2020-09-18.tar.gz.nc
   mdecrypt backup-OpenWrt-protonvpn-is-dk-2021-04-13.tar.gz.nc

Put :code:`tun-up` and :code:`tun-down` in :code:`router:/etc`

.. code::

   scp tun-up tun-down router:/etc

Put :code:`vpn-up` and :code:`vpn-down` in :code:`router:/usr/bin/`

.. code::

   scp vpn-up vpn-down router:/usr/bin
